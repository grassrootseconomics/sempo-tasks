"""Sempo API tasks

.. moduleauthor:: Louis Holbrook <dev@holbrook.no>

.. todo::
    The Sempo API tasks should be moved to the sempo_client package, and an interface should be defined that this module can implement.

"""

import celery

from .token import *
