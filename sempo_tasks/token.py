# standard imports
import os
import logging

# third-party imports
import celery

# platform imports
from cic_registry import ContractRegistry
from sempo_client import ApiClient

logg = logging.getLogger(__file__)
celery_app = celery.current_app 

api_client = ApiClient('localhost', '9000', False, None)


def authorize(username, password):
    api_client.authorize(username, password)

@celery_app.task()
def register_token_organisation(token, announcer_address, organisation_name, country_code, timezone, email):
    """Registers a new token and corresponding organisation on the Sempo platform.

    :params token: Ethereum contract address of ERC20 token to add
    :type token: str, 0x-hex
    :param announcer_address: Ethereum address of ERC20 token "owner"
    :type announcer_address: str, 0x-hex
    :param organisation_name: Common name of organisation to use in registration
    :type organisation_name: str
    :param country_code: ISO-3166 alpha-2 country code of organisation
    :type country_code: str
    :param timezone: Timezone of organisation, specified as tzdata
    :type timezone: str
    :param email: Organisation email for purposes of registration
    :type email: str
    """
    token_address = token['address']
    converter_address = token['converters'][0]
    abi = ContractRegistry.abi('ERC20Token')
    c = w3.eth.contract(abi=abi, address=token_address)

    token_name = c.functions.name().call()
    token_symbol = c.functions.symbol().call()
    token_decimals = c.functions.decimals().call()

    o = {
       	"name": token_name,
	"symbol": token_symbol,
	"decimals": token_decimals,
	"address": token_address,
	"is_reserve": False,
    }

    r = api_client.post('/token/', o)
    token_sempo_id = r['data']['token']['id']
   
    o = {
	"organisation_name": organisation_name,
	"custom_message_welcome_key": '{}_org'.format(organisation_name),
	"timezone": timezone,
	"country_code": country_code,
	"account_address": announcer_address,
	"token_id": token_sempo_id,
    }
    logg.debug('org {}'.format(o))
    r = api_client.post('/organisation/', o)
    logg.debug('org result {}'.format(r))
    organisation_sempo_id = r['data']['organisation']['id']


    o = {
        "organisation_id": organisation_sempo_id,
        "address": announcer_address,
            }
    r = api_client.post('/transfer_account/register/', o, 2)
    logg.debug('account result {}'.format(r))


    o = {
        'email': email,
        'tier': 'admin',
        'organisation_id': organisation_sempo_id,
            }
    r = api_client.post('/auth/permissions/', o)
    logg.debug('auth permissions result {}'.format(r))


    registry_address = ContractRegistry.get_contract('ContractRegistry').address
    reserve = ContractRegistry.get_default_reserve()
    converter_abi = ContractRegistry.abi('ConverterBase')
    converter_contract = w3.eth.contract(abi=converter_abi, address=converter_address)
    reserve_weight = converter_contract.functions.reserveWeight(reserve.address).call()

    o = {
	"reserve_address": reserve.address,
        "token_address": token_address,
        "exchange_address": converter_address,
        "registry_address": registry_address,
        "reserve_ratio_ppm": reserve_weight,
	"organisation_id": organisation_sempo_id,
    }

    r = api_client.post('/exchange/register/', o, 2)
    logg.debug('resp {}'.format(r))


#{
#	"organisation_id": $oid,
#	"address": "$t_converter"
#}
#EOF
#`
#	r=`curl -X POST "${APP_LOCATION}/api/v2/transfer_account/register/" -d "$d" -H "Content-Type: application/json" -H "Authorization: $token"`
#	echo "transfer account results $r"
#
#
#	d=`cat <<EOF
#{
#	"email": "ge_${t_symbol}@sechost.info",
#	"tier": "admin",
#	"organisation_id": $oid
#}
#EOF
#`
#	r=`curl -X POST "${APP_LOCATION}/api/v1/auth/permissions/" -d "$d" -H "Content-Type: application/json" -H "Authorization: $token"`
#
