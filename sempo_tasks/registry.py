class LegacyContractRegistry(ContractRegistry):
    """Satisfies requirements for upstream eth_worker/* client code

    Required by Sempo tasks.
    """
    def get_contract_abi(self, contract_name):
        """Return the EVM abi of given contract

        :param contract_key: Local id of contract. If the contract exists in the Contract Registry, the id will match the name used there.
        :type contract_key: str
        :return: ABI, json
        :rtype: dict
        """
        return self.contracts[contract_name].contract.abi


    def get_contract_address(self, contract_name):
        """Return the address of given contract

        :param contract_key: Local id of contract. If the contract exists in the Contract Registry, the id will match the name used there.
        :type contract_key: str
        :return: Contract address
        :rtype: str, 0x-hex
        """
        return self.contracts[contract_name].address
