# standard imports
import os
import logging
import pytest

# third-party imports
import celery
from cic_eth.eth.bancor import resolve_tokens_by_symbol, resolve_converters_by_tokens

logg = logging.getLogger(__file__)


def test_register_token(
    init_registry,
    init_w3_nokey,
    celery_session_worker,
    sempo_api_mock,
    ):

    tokens = resolve_tokens_by_symbol([os.environ.get('CIC_ALICE_TOKEN_SYMBOL')])
#    tokens = resolve_converters_by_tokens(tokens)
#   token = tokens[0]

    s_register_token = celery.signature(
        'cic_eth.eth.token.add_token_to_registry',
        [tokens[0]['address']],
            )

    s_register_token_organisation = celery.signature(
        'cic_eth.sempo_app.token.register_token_organisation',
        [
            init_w3_nokey.eth.accounts[3],
            'Grassroots Economics',
            'KE',
            'Africa/Nairobi',
            'accounts-grassrootsecononics@holbrook.no',
            ],
            )

    s_register_token.link(s_register_token_organisation)
    t = s_register_token.apply_async()
    #s_resolve_converter.link(s_register_token)
    #t = s_resolve_converter.apply_async()
    r = t.get()
    logg.warning('kids 1 {} {}'.format(t.children, r))
    r = t.children[0].get()
